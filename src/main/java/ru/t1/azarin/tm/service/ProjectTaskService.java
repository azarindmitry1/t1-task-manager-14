package ru.t1.azarin.tm.service;

import ru.t1.azarin.tm.api.service.IProjectTaskService;
import ru.t1.azarin.tm.api.repository.IProjectRepository;
import ru.t1.azarin.tm.api.repository.ITaskRepository;
import ru.t1.azarin.tm.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;
    private final ITaskRepository taskRepository;

    public ProjectTaskService(IProjectRepository projectRepository, ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public void bindTaskToProject(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) return;
        if (taskId == null || taskId.isEmpty()) return;
        if (!projectRepository.existById(projectId)) return;
        final Task task = taskRepository.findOneById(taskId);
        if (task == null) return;
        task.setProjectId(projectId);
    }

    @Override
    public void removeProjectById(final String projectId) {
        if (projectId == null || projectId.isEmpty()) return;
        if (!projectRepository.existById(projectId)) return;
        final List<Task> tasks = taskRepository.findAll();
        for (Task task : tasks) taskRepository.removeById(task.getId());
        projectRepository.removeById(projectId);
    }

    @Override
    public void unbindTaskToProject(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) return;
        if (taskId == null || taskId.isEmpty()) return;
        if (!projectRepository.existById(projectId)) return;
        final Task task = taskRepository.findOneById(taskId);
        if (task == null) return;
        task.setProjectId(null);
    }

}